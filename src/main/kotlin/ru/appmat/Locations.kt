package ru.appmat

import io.ktor.locations.*

@Location("/health")
class Health

@Location("/ready")
class Ready


@Location("/api")
class API {
    @Location("/register/{id}")
    data class Register(val api: API, val id: String)
}
